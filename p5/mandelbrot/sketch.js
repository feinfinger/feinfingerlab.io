//noprotect
var LRSlider;
var mode = true;

function setup() {
	createCanvas(800, 800);
	pixelDensity(1);
}

function draw() {
	var maxiterations = 100;
	loadPixels();
	for (var x = 0; x < width; x++) {
		for (var y = 0; y < height; y++) {
			var a = map(x, 0, width, -2.1, 1.1);
			var b = map(y, 0, height, -1.6, 1.6);
			var ca = a;
			var cb = b;
			var n = 0;
			while (n < maxiterations) {
				var aa = a * a - b * b;
				var bb = 2 * a * b;
				a = aa + ca;
				b = bb + cb;
				if (a * a + b * b > 4) {
					break;
				}
				n++;
			}
      if(mode) {

      	var h = (n / 50)%1;

        brightR = hslToRGB(h, 1, 0.5, 1);
				brightB = hslToRGB(h, 1, 0.5, 2);
				brightG = hslToRGB(h, 1, 0.5, 3);
      }
			if (n == maxiterations) {
				brightR = 0;
				brightB = 0;
				brightG = 0;
      }
      else if (!mode) {
        brightR = 255;
				brightB = 255;
				brightG = 255;
			}
			var pix = (x + y * width) * 4;
			pixels[pix + 0] = brightR;
			pixels[pix + 1] = brightB;
			pixels[pix + 2] = brightG;
			pixels[pix + 3] = 255;
		}

	}
	updatePixels();
  noLoop();
}
//converts RGB from HSL, 4th param for RGB selection
function hslToRGB(h, s, l, RGB){
    var r, g, b;

    if(s == 0){
        r = g = b = l; // achromatic
    }else{
        var hue2rgb = function hue2rgb(p, q, t){
            if(t < 0) t += 1;
            if(t > 1) t -= 1;
            if(t < 1/6) return p + (q - p) * 6 * t;
            if(t < 1/2) return q;
            if(t < 2/3) return p + (q - p) * (2/3 - t) * 6;
            return p;
        }

        var q = l < 0.5 ? l * (1 + s) : l + s - l * s;
        var p = 2 * l - q;
        r = hue2rgb(p, q, h + 1/3);
        g = hue2rgb(p, q, h);
        b = hue2rgb(p, q, h - 1/3);
    }
		if(RGB == 1) {
    	return Math.round(r * 255);
    }
  	if(RGB == 2) {
    	return Math.round(g * 255);
    }
  	if(RGB == 3) {
    	return Math.round(b * 255);
    }
}