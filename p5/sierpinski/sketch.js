function setup() {
	createCanvas(729, 729);
	background(0);
	fill(255);
	noStroke();
}

its = 6;

function draw() {
	for (i = its; i > 0; i--) {
		for (x = pow(3, i); x > 0; x--) {
			for (y = pow(3, i); y > 0; y--) {
				if ((x - 1) % 3 == 0 && (y - 1) % 3 == 0) {
					rect(x * width / pow(3, i), y * height / pow(3, i), width / pow(3, i), height / pow(3, i));
				}
			}
		}
	}
	noLoop();
}